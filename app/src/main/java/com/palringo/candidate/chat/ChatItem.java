package com.palringo.candidate.chat;

import android.os.Parcel;
import android.os.Parcelable;

public class ChatItem implements Parcelable {
    private String avatarUrl;
    private String message;

    ChatItem(String avatarUrl, String message) {
        this.avatarUrl = avatarUrl;
        this.message = message;
    }

    String getAvatarUrl() {
        return avatarUrl;
    }

    String getMessage() {
        return message;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(avatarUrl);
        parcel.writeString(message);
    }

    public static final Parcelable.Creator<ChatItem> CREATOR = new Parcelable.Creator<ChatItem>() {
        // распаковываем объект из Parcel
        public ChatItem createFromParcel(Parcel in) {
            return new ChatItem(in);
        }
        public ChatItem[] newArray(int size) {
            return new ChatItem[size];
        }
    };
    // конструктор, считывающий данные из Parcel
    private ChatItem(Parcel parcel) {
        avatarUrl = parcel.readString();
        message = parcel.readString();
    }
}
