package com.palringo.candidate.chat;

import android.nfc.Tag;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

import com.palringo.candidate.R;

import java.util.ArrayList;
import java.util.List;

public class ChatFragment extends Fragment {
    public static final String TAG = "ChatFragment";
    private static final String AVATAR_PATH = "file:///android_asset/leopard.jpg";

    private EditText messageEditText;
    private ChatRecyclerAdapter recyclerAdapter;


    private  List<ChatItem> message = new ArrayList<>();

    private  List<ChatItem> messag = null;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ChatFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState != null){
        savedInstanceState.getParcelable("message");
        message = savedInstanceState.getParcelableArrayList("message");
        Log.d(TAG, "onCreate" + savedInstanceState);
        Log.d(TAG, "onC message " + message.size());
        messag = message;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.chat_item_list, container, false);

        View emtpyStateView = view.findViewById(R.id.empty_view);

        recyclerAdapter = new ChatRecyclerAdapter(messag, emtpyStateView);

        // Set the adapter
        RecyclerView recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter(recyclerAdapter);

        messageEditText = view.findViewById(R.id.typedMessage);
        ImageButton sendButton = view.findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recyclerAdapter.addMessage(AVATAR_PATH, messageEditText.getText().toString());
                message.add(new ChatItem(AVATAR_PATH, messageEditText.getText().toString()));
                messageEditText.setText("");


                Log.d(TAG, "message added:" + message.size());
            }
        });

        return view;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList("message", (ArrayList<? extends Parcelable>) message);
        Log.d(TAG, "onSaveInstSta" + outState);
    }

}



